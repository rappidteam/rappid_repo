<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Cube Summation</title>
        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
                height: auto;
            }

            .container {
                background-color: #f1f1f1;
                width: 100%;
                overflow: auto;
                position: absolute;
                top: 100px;
                bottom: 0;
                height: auto;
               
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            .textareacontainer, .iframecontainer {
                float: left;
                height: 100%;
                width: 50%;
                padding-left: 1.5%;
                padding-right: 0.75%;
            }

            .headerText {
                width: auto;
                float: left;
                font-family: verdana;
                font-size: 1em;
                line-height: 2;
            }

            .iframewrapper {
                width: 100%;
                height: 92%;
                -webkit-overflow-scrolling: touch;
                background-color: #fffaaa;
                box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            }

            .textareawrapper {
                width: 100%;
                height: 100%;
                background-color: #ffffff;
                position: relative;
                box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            }

            .testcasesarea {
                background-color: #ffffff;
                font-family: consolas,"courier new",monospace;
                font-size: 15px;
                height: 100%;
                width: 100%;
                padding: 8px;
                resize: none;
                border: none;
                line-height: normal;
                position: relative;

            }

            .testcasesareaout {
                background-color: #fffaaa;
                font-family: consolas,"courier new",monospace;
                font-size: 15px;
                height: 100%;
                width: 100%;
                padding: 8px;
                resize: none;
                border: none;
                line-height: normal;
                position: relative;

            }

            #errors {
                font-family: consolas,"courier new",monospace;
                font-size: 15px;
                color:red;
            }

        </style>

          <script>
             function sendData(){
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });
                $("#errors").html("");
                 $("#outputArea").html("");
                $.ajax({
                   type:'POST',
                   url:'./processCube',
                   data:{testCases : $('#testCases').val()},
                   success:function(data){
                        
                        var printData = "";
                        for (var i = 0; i < data.length; i++) {
                                printData = printData + data[i] + "\n";
                        }
                        $("#outputArea").html(printData);

                   },
                   error:function(data){
                        var JSONResponse = data.responseJSON;
                        var errorOutput = "<ul>";   
                        for (var i = 0; i < JSONResponse.length; i++) {
                                errorOutput = errorOutput + "<li>" + JSONResponse[i] + "</li>";
                        }

                        errorOutput = errorOutput + "</ul>"; 
                        $("#errors").html(errorOutput);
                    }
                });
             }
          </script>
    </head>
    <body>
        <div id="errors"></div>
        <div class="container">
            <div class="textareacontainer">
                <div style="overflow:auto; min-height: 50px">
                    <div class="headerText">
                        <button id="sendDataBtn" class="w3-btn w3-teal w3-border w3-border-light-blue" onclick="sendData();">Run Tests Cases</button>
                    </div>
                </div>
                <div class="iframewrapper">
                    
                        <div class="textareawrapper">
                            <textarea wrap="logical" id="testCases" class="testcasesarea"></textarea>
                        </div>
                </div>
            </div>
            <div class="textareacontainer">
               <div style="overflow:auto; min-height: 50px">
                    <div class="headerText">Output</div>
                </div>
                <div class="iframewrapper">
                    <div class="textareawrapper">
                        <textarea wrap="logical" id="outputArea" class="testcasesareaout" readonly="true"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
