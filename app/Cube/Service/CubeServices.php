<?php

namespace App\Cube\Service;

use Exception;

use App\Cube\Model\CubeTestCase;
use App\Cube\Model\Cube;

class CubeServices 
{
    
    public function __construct () 
    {
    	
    }

    public function executeTestCase(CubeTestCase $testCase) 
    {
    	$cube = new Cube($testCase->getCubeSize());
    	$responses = array();

    	foreach ($testCase->getOperations() as $key => $operation) {
    		$params = $operation[key($operation)];
    		
    		switch (key($operation)) {
    			case CubeTestCase::UPDATE:
    				$cube->update(intval($params[0]), intval($params[1]), intval($params[2]), doubleval($params[3]));
    				break;
    			
    			case CubeTestCase::QUERY:
    				$responses[] = $cube->sumBlock(intval($params[0]), intval($params[1]), intval($params[2]), 
    					intval($params[3]), intval($params[4]), intval($params[5]));
    				break;
    			default;
    				break;
    		}
    	}
    	
    	return $responses;
    }

    public function parseTestCases($stringInput) 
    {
    	$inputLines = explode("\n", $stringInput);

    	$testCasesQuantity = intval(array_shift($inputLines)); //T

    	if($testCasesQuantity < 1)
    	{
    		throw new Exception("Test cases quantity (fisrt line of input) should be great than 1");
    	}

    	$testCases = array();

    	$i = 0;
    	while ($i < $testCasesQuantity)
    	{
    		$i++;
    		$testCasefirstLine = explode(" ", array_shift($inputLines));

    		if(count($testCasefirstLine) != 2 || !is_numeric($testCasefirstLine[0]) || !is_numeric($testCasefirstLine[1]))
    		{
				throw new Exception("First Line of test case cube size and operations quantity shoulb be N M. eg: 4 5");
    		}
    		
    		$testCase = new CubeTestCase();
    		$testCase->setCubeSize(intval($testCasefirstLine[0])); //N

    		$queriesQuantity = intval($testCasefirstLine[1]);  //M

    		for ($j=0; $j < $queriesQuantity; $j++) 
    		{ 	
    			$testCase->addOperation(array_shift($inputLines));

    		}
    		$testCases[] = $testCase;

    	}

    	return $testCases;
    }


}