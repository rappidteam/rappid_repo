<?php

namespace App\Cube\Model;

use Exception;

class CubeTestCase 
{
	const UPDATE = "UPDATE";
    const QUERY = "QUERY"; 

    private $cubeSize;
    private $operations = array(); 

    public function __construct () 
    {
    	
	}

    public function setCubeSize($cubeSize)
    {
        $this->cubeSize = $cubeSize;
    }

    public function getCubeSize()
    {
        return $this->cubeSize;
    }

    public function setOperations($operations)
    {
        $this->operations = $operations;
    }

    public function getOperations()
    {
        return $this->operations;
    }

    public function addOperation($operationText)
    {
        $operationArray = explode (" ", $operationText, 2);
        
        $operationParams = array(); 
        switch ($operationArray[0]) 
        {
            case CubeTestCase::UPDATE:
                $operationParams = explode (" ", $operationArray[1]);
                if (count ($operationParams) != 4)
                {
                    throw new Exception("Wrong # of parameters for operation ".$operationArray[0]. " eg: UPDATE 2 2 2 1");
                }
                break;

            case CubeTestCase::QUERY:
                $operationParams = explode (" ", $operationArray[1]);
                if (count ($operationParams) != 6)
                {
                    throw new Exception("Wrong # of parameters for operation ".$operationArray[0]. " eg: QUERY 1 1 1 1 1 1");
                }
                break;

            default:
                throw new Exception("Operation ".$operationArray[0]." not recognized, values accepted: UPDATE, QUERY");
        }

        if(!$this->areParametersValid($operationParams))
        {
            throw new Exception("Operation ".$operationArray[0]." parameters (".$operationArray[1].") should be numbers");
        }

        $this->operations[] = array($operationArray[0]=>$operationParams);

    }

    private function areParametersValid($params)
    {   
        for ($i=0; $i < count($params); $i++) 
        {   
            if (!is_numeric($params[$i]))
            {
                return false;
            }
        }
        
        return true;
    }

}