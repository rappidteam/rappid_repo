<?php

namespace App\Cube\Model;

use Exception;

class Cube 
{
	private $minSize = 1;
	private $maxSize = 100;
	private $valueLimit = 1000000000;
	private $matrixSize = 0;
    private $matrix = array();

    public function __construct ($size) 
    {
    	if (!is_int($size) || $size < $this->minSize || $size > $this->maxSize) 
    	{
    		throw new Exception("Matrix size should be an integer great or equal than ". $this->minSize .
    			" and equal or less than ". $this->maxSize);
    	}
    	
    	$this->matrixSize = $size;
		
    	$this->matrix = array(array(array()));
    	for ($i = 0; $i < $this->matrixSize; $i++) 
    	{ 
			for ($j = 0; $j < $this->matrixSize; $j++) 
			{ 
    			for ($k = 0; $k < $this->matrixSize; $k++) 
    			{ 
    				$this->matrix[$i][$j][$k] = 0;
    			}
    		}    		
    	}
	}

    public function getMatrix() 
    {
    	return $this->matrix;
    }

    public function update($x, $y, $z, $newValue) 
    {
    	if (!is_int($x) || $x < $this->minSize || $x > $this->matrixSize ||
    		!is_int($y) || $y < $this->minSize || $y > $this->matrixSize || 
    		!is_int($z) || $z < $this->minSize || $z > $this->matrixSize) 
    	{
    		throw new Exception("x, y or z could should be between ". $this->minSize . " and ". $this->matrixSize);
    	}

    	if (!is_numeric($newValue) || $newValue < (-1*$this->valueLimit) || $newValue > $this->valueLimit )
    	{
    		throw new Exception("newValue should be an integer" );
    	}
    	 
    	$this->matrix[$x-1][$y-1][$z-1] = $newValue;
    }

    public function sumBlock($x1, $y1, $z1 ,$x2, $y2, $z2)
    {
    	if (!is_int($x1) || !is_int($x2) || $x1 < $this->minSize || $x2 > $this->matrixSize || $x1 > $x2 ||
    		!is_int($y1) || !is_int($y2) || $y1 < $this->minSize || $y2 > $this->matrixSize || $y1 > $y2 || 
    		!is_int($z1) || !is_int($z2) || $z1 < $this->minSize || $z2 > $this->matrixSize || $z1 > $z2) 
    	{
    		throw new Exception("x, y or z could should be between ". $this->minSize . " and ". $this->matrixSize);
    	}
    	
    	$sum = 0;
    	for ($i = $x1-1; $i < $x2; $i++) 
    	{ 
			for ($j = $y1-1; $j < $y2; $j++) 
			{ 
    			for ($k = $z1-1; $k < $z2; $k++) 
    			{ 
    				$sum = $sum + $this->matrix[$i][$j][$k];
    			}
    		}    		
    	}
    	
    	return $sum;
    }

}