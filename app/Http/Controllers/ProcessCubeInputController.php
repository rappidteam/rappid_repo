<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cube\Model\Cube;
use App\Cube\Model\CubeTestCase;
use App\Cube\Service\CubeServices;

use Exception;



class ProcessCubeInputController extends Controller
{
    public function processInput(Request $request) 
    {
    	$responses = array();
    	try {
    		$cubeServices = new CubeServices();
    		$testCases = $cubeServices->parseTestCases($request->testCases);
    		
    		foreach ($testCases as $key => $value) {
    			$responsesTmp = $cubeServices->executeTestCase($value);

    			$responses = array_merge($responses, $responsesTmp);
    		}

    	}
    	catch(Exception $e) {
		  return response()->json(array($e->getMessage()), 400);
		}	
    	return response()->json($responses, 200);
    }
}
