<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Cube\Service\CubeServices;
use App\Cube\Model\CubeTestCase;

class CubeServicesTest extends TestCase
{
    public function testParseTestCases()
    {
    	$text = "1\n1 2\nUPDATE 1 1 1 4\nQUERY 1 1 1 1 1 1";
		
    	$testCase = new CubeTestCase();
    	$testCase->setCubeSize(1);
    	
    	$operations = array(array("UPDATE" => array(1, 1, 1, 4)), array("QUERY" => array(1, 1, 1, 1, 1, 1)));
    	$testCase->setOperations($operations);

    	$testCases = array();
		$testCases[] = $testCase;

		$cubeServices = new CubeServices();
		
		$testCasesReturn = $cubeServices->parseTestCases($text);

        $this->assertEquals($testCases, $testCasesReturn);
    }


    /**
	* @expectedException Exception
	*/
    public function testParseTestCasesWrongInput()
    {
    	$text = "adadadasd";
		
    	$cubeServices = new CubeServices();
		
		$testCasesReturn = $cubeServices->parseTestCases($text);
	}
}
