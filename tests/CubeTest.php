<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Cube\Model\Cube;
//Test case Cubetest
class CubeTest extends TestCase
{
    public function testCreateCube() 
    {
    	$matrix = array(array(array(0)));
    	$cube = new Cube(1);
       	$matrixReturned = $cube->getMatrix();

       	$this->assertEquals($matrix, $matrixReturned);

       	
       	$matrix = array(
    					array(
    						array(0,0), 
    						array(0,0)), 
    					array(
    						array(0,0), 
    						array(0,0))
    				);
		$cube = new Cube(2);
       	$matrixReturned = $cube->getMatrix();

       	$this->assertEquals($matrix, $matrixReturned);
    }


    /**
	* @expectedException Exception
	*/
    public function testCreateCubeException()
    {
    	$cube = new Cube('a');
    }

	/**
	* 
	*/
    public function testUpdateCube() 
    {
    	$matrix = array(
    					array(
    						array(0,0,0), 
    						array(0,0,0),
    						array(0,0,0)), 
    					array(
    						array(0,0,0), 
    						array(0,0,1),
    						array(0,0,0)),
    					array(
    						array(0,0,0), 
    						array(0,0,0),
    						array(0,0,0))
    				);

        $cube = new Cube(3);

        $cube->update(2, 2, 3, 1);
       
        $matrixReturned = $cube->getMatrix();

        $this->assertEquals($matrix, $matrixReturned);


        $matrix = array(
    					array(
    						array(0,0,0), 
    						array(0,0,0),
    						array(0,0,0)), 
    					array(
    						array(0,0,0), 
    						array(0,0,1),
    						array(0,0,0)),
    					array(
    						array(0,0,0), 
    						array(0,4,0),
    						array(0,0,0))
    				);

       	 $cube->update(3, 2, 2, 4);

       	 $matrixReturned = $cube->getMatrix();

        $this->assertEquals($matrix, $matrixReturned);
    }

    /**
	* 
	*/
    public function testUpdateCubeDoubleValue() 
    {
    	$matrix = array(
    					array(
    						array(0,0,0), 
    						array(0,0,0),
    						array(0,0,0)), 
    					array(
    						array(0,0,0), 
    						array(0,0,3.6),
    						array(0,0,0)),
    					array(
    						array(0,0,0), 
    						array(0,0,0),
    						array(0,0,0))
    				);

        $cube = new Cube(3);

        $cube->update(2, 2, 3, 3.6);
       
        $matrixReturned = $cube->getMatrix();

        $this->assertEquals($matrix, $matrixReturned);

        $this->assertEquals($matrix, $matrixReturned);
    }



    /**
	* @expectedException Exception
	*/
    public function testUpdateCubeExceptionXLimit()
    {
    	$cube = new Cube(1);

    	$cube->update(2, 1, 1, 3);
    }

    /**
	* @expectedException Exception
	*/
    public function testUpdateCubeExceptionYLimit()
    {
    	$cube = new Cube(1);

    	$cube->update(1, 2, 1, 3);
    }

    /**
	* @expectedException Exception
	*/
    public function testUpdateCubeExceptionZLimit()
    {
    	$cube = new Cube(1);

    	$cube->update(1, 1, 2, 3);
    }

    /**
	* @expectedException Exception
	*/
    public function testUpdateCubeValueExceptionMinValue()
    {
    	$cube = new Cube(1);
    	$newValue = pow(10,9)*-1;
    	$cube->update(1, 1, 1, $newValue-1);
    }

    /**
	* @expectedException Exception
	*/
    public function testUpdateCubeValueExceptionMaxValue()
    {
    	$cube = new Cube(1);

    	$newValue = pow(10,9);
    	$cube->update(1, 1, 1, $newValue+1);
    }

    public function testCubeSumBlock()
    {
    	$cube = new Cube(4);
    	$cube->update(2, 2, 2, 4);
    	$returnValue = $cube->sumBlock(2, 2, 2, 4, 4, 4);
    	$this->assertEquals(4, $returnValue);

    	$cube->update(1, 1, 1, 23);
    	$returnValue = $cube->sumBlock(2, 2, 2, 4, 4, 4);
    	$this->assertEquals(4, $returnValue);

    	$returnValue = $cube->sumBlock(1, 1, 1, 3, 3, 3);
    	$this->assertEquals(27, $returnValue);
    
    }

    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionXMinPos()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(-1, 1, 1, 2, 2, 2, 2);
    }

    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionXMaxPos()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(1, 1, 1, 3, 2, 2, 2);
    }


    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionX1MaxThanX2()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(2, 1, 1, 1, 2, 2, 2);
    }

    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionYMinPos()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(1, -1, 1, 2, 2, 2, 2);
    }

    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionYMaxPos()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(1, 1, 1, 2, 3, 2, 2);
    }


    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionY1MaxThanY2()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(1, 2, 1, 2, 1, 2, 2);
    }

    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionZMinPos()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(1, 1, -1, 2, 2, 2, 2);
    }

    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionZMaxPos()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(1, 1, 1, 2, 2, 3, 2);
    }


    /**
	* @expectedException Exception
	*/
    public function testCubeSumBlockExceptionZ1MaxThanZ2()
    {
    	$cube = new Cube(2);

    	$cube->sumBlock(1, 1, 2, 2, 2, 1, 2);
    }

}
