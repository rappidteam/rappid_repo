<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Cube\Model\CubeTestCase;

class CubeTestCaseTest extends TestCase
{

    public function testAddUpdateOperation()
    {
    	$operations = array(array("UPDATE"=>array(2, 2, 2, 4))); 

    	$cubeTestCase = new CubeTestCase();

    	$cubeTestCase->addOperation("UPDATE 2 2 2 4");

    	$operationsReturned = $cubeTestCase->getOperations();

    	$this->assertEquals($operations, $operationsReturned);
    }

    /**
	* @expectedException Exception
	*/
    public function testAddUpdateOperationWrongParamsQuantity()
    {
    	$cubeTestCase = new CubeTestCase();

    	$cubeTestCase->addOperation("UPDATE 2 2 2 4 2");
    }

    public function testAddQueryOperation()
    {
    	$operations = array(array("QUERY"=>array(1, 1, 1, 3, 3, 3))); 

    	$cubeTestCase = new CubeTestCase();

    	$cubeTestCase->addOperation("QUERY 1 1 1 3 3 3");

    	$operationsReturned = $cubeTestCase->getOperations();

    	$this->assertEquals($operations, $operationsReturned);
    }

    /**
	* @expectedException Exception
	*/
    public function testAddQueryOperationWrongParamsQuantity()
    {
    	$cubeTestCase = new CubeTestCase();

    	$cubeTestCase->addOperation("QUERY 1 1 1 3 3 3 1");
    }
}
